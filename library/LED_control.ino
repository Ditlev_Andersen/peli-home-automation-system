#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "PELI_Sandbox";
const char* password = "PELIpeli";
const char* mqtt_server = "192.168.1.110";
const char* mqtt_topic = "home/appliance/dryer";

unsigned long startTime = 0;
const int ledPin = 10;
WiFiClient espClient;
PubSubClient client(espClient);
bool Activated = false;

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String message = "";

  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    message += (char)payload[i];
  }


  // Check the received message
  if (message == "true") {
    // Rotate the servo to angle
    startTime = millis();
    Activated = true;



  } else if (message == "off") {
    Activated = false;
    client.publish("home/appliance/dryer", "stop");
    digitalWrite(2, HIGH);
    // Rotate the servo to another angle (adjust as needed)
    // 0 degrees
    Serial.println("LEDOFF");
  } else {
    Serial.println("Ledoff");
  }
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.subscribe(mqtt_topic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  pinMode(2, OUTPUT);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  if (Activated == true) {
    digitalWrite(2, LOW);
    Serial.println("led");
  }

  if (millis() - startTime >= 30000 && Activated == true) {
    // Serial.println("disconneting");
    // Stop the servo
    digitalWrite(2, HIGH);
    Activated = false;
    // Publish "done" topic
    client.publish("home/appliance/dryer", "stop ");
  }
}