// #include <ESP8266WiFi.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include <ezButton.h>
#include <driver/adc.h>

// Define the 4 buttons
#define WASHINGMACHINE_BUTTON 12//26
#define DRYER_BUTTON 13 //27
#define OVEN_BUTTON 16
#define OTHER_BUTTON 17

// Define 2 LED ports for each button
#define DRYER_ACTIVATED_LED 32  
#define DRYER_STARTED_LED 33

#define WASHINGMACHINE_ACTIVATED_LED 26
#define WASHINGMACHINE_STARTED_LED 27

#define OVEN_ACTIVATED_LED 19
#define OVEN_STARTED_LED 18 

#define OTHER_ACTIVATED_LED 23 
#define OTHER_STARTED_LED 22 

// LED port for power on/off button
#define ON_OFF_LED 21

// ADC and GPIO for push button power
#define ADC_PIN 25
#define BUTTON_POWER_INPUT 14
#define POWER_OUTPUT 4

int voltage = 1;

int reading_power_button = HIGH;
bool released = 0;

// Define button states for debouncing
int buttonStateWashingmachine = HIGH;          
int lastButtonStateWashingmachine = HIGH; 

int buttonStateDryer = HIGH;           
int lastButtonStateDryer = HIGH; 

int buttonStateOven = HIGH;           
int lastButtonStateOven = HIGH;

int buttonStateOther = HIGH;    
int lastButtonStateOther = HIGH;

// Housekeeping for on/off circuit
bool isActive = false;
int startTime = millis();

// MQTT topics
const char dryerTopic[] = "home/appliance/dryer";
const char washerTopic[] = "home/appliance/washer";
const char ovenTopic[] = "home/appliance/oven";
const char otherTopic[] = "home/appliance/other";


// Unsigned long since time in mm will become large number
// Used for debouncing
unsigned long lastDebounceTimeWashingmachine = 0; 
unsigned long lastDebounceTimeDryer = 0;
unsigned long lastDebounceTimeOven = 0;
unsigned long lastDebounceTimeOther = 0;
unsigned long debounceDelay = 50;

// MQTT server and WiFi
const char* ssid = "PELI_Sandbox";
const char* password = "PELIpeli";
const char* mqtt_server = "192.168.1.110";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String messageTemp;

  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    messageTemp += (char)payload[i];
  }

  // Logic for washingmachine status LEDS
  if (String(topic) == washerTopic) {
    // Toggle 'activated' or 'started' LED according to message 
    if(messageTemp == "on"){
      digitalWrite(WASHINGMACHINE_ACTIVATED_LED, HIGH);
    }
    else if(messageTemp == "off"){
      digitalWrite(WASHINGMACHINE_ACTIVATED_LED, LOW);
      digitalWrite(WASHINGMACHINE_STARTED_LED, LOW);
    }
    else if(messageTemp == "start") {
      digitalWrite(WASHINGMACHINE_STARTED_LED, HIGH);
    }
    else if(messageTemp == "stop") {
      digitalWrite(WASHINGMACHINE_STARTED_LED, LOW);
      digitalWrite(WASHINGMACHINE_ACTIVATED_LED, LOW);
    }
  }

  // Logic for dryer status LEDS
  if (String(topic) == dryerTopic) {
    // Toggle 'activated' or 'started' LED according to message 
    if(messageTemp == "on"){
      digitalWrite(DRYER_ACTIVATED_LED, HIGH);
    }
    else if(messageTemp == "off"){
      digitalWrite(DRYER_ACTIVATED_LED, LOW);
      digitalWrite(DRYER_STARTED_LED, LOW);
    }
    else if(messageTemp == "start") {
      digitalWrite(DRYER_STARTED_LED, HIGH);
    }
    else if(messageTemp == "stop") {
      digitalWrite(DRYER_STARTED_LED, LOW);
      digitalWrite(DRYER_ACTIVATED_LED, LOW);
    }
  }

  // Logic for oven status LEDS
  if (String(topic) == ovenTopic) {
    // Toggle 'activated' or 'started' LED according to message 
    if(messageTemp == "on"){
      digitalWrite(OVEN_ACTIVATED_LED, HIGH);
    }
    else if(messageTemp == "off"){
      digitalWrite(OVEN_ACTIVATED_LED, LOW);
      digitalWrite(OVEN_STARTED_LED, LOW);
    }
    else if(messageTemp == "start") {
      digitalWrite(OVEN_STARTED_LED, HIGH);
    }
    else if(messageTemp == "stop") {
      digitalWrite(OVEN_STARTED_LED, LOW);
      digitalWrite(OVEN_ACTIVATED_LED, LOW);
    }
  }

  // Logic for other status LEDS
  if (String(topic) == otherTopic) {
    // Toggle 'activated' or 'started' LED according to message 
    if(messageTemp == "on"){
      digitalWrite(OTHER_ACTIVATED_LED, HIGH);
    }
    else if(messageTemp == "off"){
      digitalWrite(OTHER_ACTIVATED_LED, LOW);
      digitalWrite(OTHER_STARTED_LED, LOW);
    }
    else if(messageTemp == "start") {
      digitalWrite(OTHER_STARTED_LED, HIGH);
    }
    else if(messageTemp == "stop") {
      digitalWrite(OTHER_STARTED_LED, LOW);
      digitalWrite(OTHER_ACTIVATED_LED, LOW);
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      client.publish(washerTopic, "hello washer");
      client.publish(dryerTopic, "hello dryer");
      client.publish(ovenTopic, "hello oven");
      client.publish(otherTopic, "hello other");
      // ... and resubscribe
      client.subscribe(washerTopic);
      client.subscribe(dryerTopic);
      client.subscribe(ovenTopic);
      client.subscribe(otherTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  // Setup input from power button
  pinMode(BUTTON_POWER_INPUT, INPUT_PULLUP);
  pinMode(POWER_OUTPUT, OUTPUT);
  digitalWrite(POWER_OUTPUT, HIGH);
  pinMode(ON_OFF_LED, OUTPUT);
  digitalWrite(ON_OFF_LED, HIGH);

  // Setup buttons for controlling appliances
  pinMode(WASHINGMACHINE_BUTTON, INPUT_PULLUP);
  pinMode(DRYER_BUTTON, INPUT_PULLUP);
  pinMode(OVEN_BUTTON, INPUT_PULLUP);
  pinMode(OTHER_BUTTON, INPUT_PULLUP);

  // Setup LED pins
  // Washingmachine LEDs
  pinMode(WASHINGMACHINE_ACTIVATED_LED, OUTPUT); 
  pinMode(WASHINGMACHINE_STARTED_LED, OUTPUT);

  // Dryer LEDs
  pinMode(DRYER_ACTIVATED_LED, OUTPUT);
  pinMode(DRYER_STARTED_LED, OUTPUT);

  // Other appliances LEDs
  pinMode(OVEN_ACTIVATED_LED, OUTPUT);
  pinMode(OVEN_STARTED_LED, OUTPUT);

  pinMode(OTHER_ACTIVATED_LED, OUTPUT);
  pinMode(OTHER_STARTED_LED, OUTPUT);

  // Setup on/off LED indicator
  pinMode(ON_OFF_LED, OUTPUT);

  pinMode(ADC_PIN, INPUT_PULLDOWN);
  
  // Initialize serial connection to PC and set MQTT callback
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

// For debouncing each button
void debounce(int reading, int pin) 
{
  switch (pin)
  {
    case WASHINGMACHINE_BUTTON:
      if (reading != lastButtonStateWashingmachine) 
      {
        // reset the debouncing timer
        lastDebounceTimeWashingmachine = millis();
      }

      if ((millis() - lastDebounceTimeWashingmachine) > debounceDelay) 
      {
        // If button have been held down for 5 seconds, start the washer
        if((millis() - lastDebounceTimeWashingmachine) > 3000 && reading == LOW)
        {
          if(!digitalRead(WASHINGMACHINE_STARTED_LED))
          {
            // Turn on 'started' LED and publish to start
            lastDebounceTimeWashingmachine = millis();
            digitalWrite(WASHINGMACHINE_STARTED_LED, HIGH);
            digitalWrite(WASHINGMACHINE_ACTIVATED_LED, HIGH);
            client.publish(washerTopic, "on");
            break;
          }
          else if (digitalRead(WASHINGMACHINE_STARTED_LED))
          {
            // Turn on 'started' LED and publish to start
            lastDebounceTimeWashingmachine = millis();
            digitalWrite(WASHINGMACHINE_STARTED_LED, LOW);
            digitalWrite(WASHINGMACHINE_ACTIVATED_LED, LOW);
            client.publish(washerTopic, "off");
            break;
          }
        }
        // if the button state has changed:
        if (reading != buttonStateWashingmachine) 
        {
          buttonStateWashingmachine = reading;
          // only toggle the LED if the new button state is LOW and the washingmachine is not running. 
          if (buttonStateWashingmachine == LOW && !digitalRead(WASHINGMACHINE_STARTED_LED)) 
          {
            if(digitalRead(WASHINGMACHINE_ACTIVATED_LED) == 1)
            {
              digitalWrite(WASHINGMACHINE_ACTIVATED_LED, LOW);
              client.publish(washerTopic, "deactivate");
            }
            else if(digitalRead(WASHINGMACHINE_ACTIVATED_LED) == 0)
            {
              client.publish("debug", "Den skal ikke herind >:(");
              char str[8];
              itoa( buttonStateWashingmachine, str, 10 );
              client.publish("debug", str);
              digitalWrite(WASHINGMACHINE_ACTIVATED_LED, HIGH);
              client.publish(washerTopic, "activate");
            }
          }
        }
      }
      break;
    
    case DRYER_BUTTON:
      if (reading != lastButtonStateDryer) 
      {
        // reset the debouncing timer
        lastDebounceTimeDryer = millis();
      }

      if ((millis() - lastDebounceTimeDryer) > debounceDelay) 
      {
        // If button have been held down for 5 seconds, start the washer
        if((millis() - lastDebounceTimeDryer) > 3000 && reading == LOW)
        {
          if(!digitalRead(DRYER_STARTED_LED))
          {
            // Turn on 'started' LED and publish to start
            lastDebounceTimeDryer = millis();
            digitalWrite(DRYER_STARTED_LED, HIGH);
            digitalWrite(DRYER_ACTIVATED_LED, HIGH);
            client.publish(dryerTopic, "on");
            break;
          }
          else if (digitalRead(DRYER_STARTED_LED))
          {
            // Turn on 'started' LED and publish to start
            lastDebounceTimeDryer = millis();
            digitalWrite(DRYER_STARTED_LED, LOW);
            digitalWrite(DRYER_ACTIVATED_LED, LOW);
            client.publish(dryerTopic, "off");
            break;
          }
        }
        // if the button state has changed:
        if (reading != buttonStateDryer) 
        {
          buttonStateDryer = reading;
          // only toggle the LED if the new button state is HIGH and the Dryer is not running. 
          if (buttonStateDryer == LOW && !digitalRead(DRYER_STARTED_LED)) 
          {
            if(digitalRead(DRYER_ACTIVATED_LED) == 1)
            {
              digitalWrite(DRYER_ACTIVATED_LED, LOW);
              client.publish(dryerTopic, "deactivate");
            }
            else if(digitalRead(DRYER_ACTIVATED_LED) == 0)
            {
              digitalWrite(DRYER_ACTIVATED_LED, HIGH);
              client.publish(dryerTopic, "activate");
            }
          }
        }
      }
      break;
    
    case OVEN_BUTTON:
      if (reading != lastButtonStateOven) 
      {
        // reset the debouncing timer
        lastDebounceTimeOven = millis();
      }

      if ((millis() - lastDebounceTimeOven) > debounceDelay) 
      {
        // If button have been held down for 5 seconds, start the washer
        if((millis() - lastDebounceTimeOven) > 3000 && reading == LOW)
        {
          if(!digitalRead(OVEN_STARTED_LED))
          {
            // Turn on 'started' LED and publish to start
            lastDebounceTimeOven = millis();
            digitalWrite(OVEN_STARTED_LED, HIGH);
            digitalWrite(OVEN_ACTIVATED_LED, HIGH);
            client.publish(ovenTopic, "on");
            break;
          }
          else if (digitalRead(OVEN_STARTED_LED))
          {
            // Turn on 'started' LED and publish to start
            lastDebounceTimeOven = millis();
            digitalWrite(OVEN_STARTED_LED, LOW);
            digitalWrite(OVEN_ACTIVATED_LED, LOW);
            client.publish(ovenTopic, "off");
            break;
          }
        }
        // if the button state has changed:
        if (reading != buttonStateOven) 
        {
          buttonStateOven = reading;
          // only toggle the LED if the new button state is HIGH and the Oven is not running. 
          if (buttonStateOven == LOW && !digitalRead(OVEN_STARTED_LED)) 
          {
            if(digitalRead(OVEN_ACTIVATED_LED) == 1)
            {
              digitalWrite(OVEN_ACTIVATED_LED, LOW);
              client.publish(ovenTopic, "deactivate");
            }
            else if(digitalRead(OVEN_ACTIVATED_LED) == 0)
            {
              digitalWrite(OVEN_ACTIVATED_LED, HIGH);
              client.publish(ovenTopic, "activate");
            }
          }
        }
      }
      break;

    case OTHER_BUTTON:
      if (reading != lastButtonStateOther) 
      {
        // reset the debouncing timer
        lastDebounceTimeOther = millis();
      }

      if ((millis() - lastDebounceTimeOther) > debounceDelay) 
      {
        // If button have been held down for 5 seconds, start the washer
        if((millis() - lastDebounceTimeOther) > 3000 && reading == LOW)
        {
          if(!digitalRead(OTHER_STARTED_LED))
          {
            // Turn on 'started' LED and publish to start
            lastDebounceTimeOther = millis();
            digitalWrite(OTHER_STARTED_LED, HIGH);
            digitalWrite(OTHER_ACTIVATED_LED, HIGH);
            client.publish(otherTopic, "on");
            break;
          }
          else if (digitalRead(OTHER_STARTED_LED))
          {
            // Turn on 'started' LED and publish to start
            lastDebounceTimeOther = millis();
            digitalWrite(OTHER_STARTED_LED, LOW);
            digitalWrite(OTHER_ACTIVATED_LED, LOW);
            client.publish(otherTopic, "off");
            break;
          }
        }
        // if the button state has changed:
        if (reading != buttonStateOther) 
        {
          buttonStateOther = reading;
          // only toggle the LED if the new button state is HIGH and the Other is not running. 
          if (buttonStateOther == LOW && !digitalRead(OTHER_STARTED_LED)) 
          {
            if(digitalRead(OTHER_ACTIVATED_LED) == 1)
            {
              digitalWrite(OTHER_ACTIVATED_LED, LOW);
              client.publish(otherTopic, "deactivate");
            }
            else if(digitalRead(OTHER_ACTIVATED_LED) == 0)
            {
              digitalWrite(OTHER_ACTIVATED_LED, HIGH);
              client.publish(otherTopic, "activate");
            }
          }
        }
      }
      break;

    default:
      break;
  }
}

// Shut off when voltage gets too low. Does not work now :(
void power_control()
{
  Serial.print("Analog value: ");
  Serial.print(voltage);
  voltage = analogRead(ADC_PIN);
  char str[8];
  itoa( voltage, str, 10 );
  client.publish("outTopic", str);

} 

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // Get reading from button pins
  int reading_washingmachine = digitalRead(WASHINGMACHINE_BUTTON);
  int reading_dryer = digitalRead(DRYER_BUTTON);
  int reading_oven = digitalRead(OVEN_BUTTON);
  int reading_other = digitalRead(OTHER_BUTTON);

  // Act according to button presses
  debounce(reading_washingmachine, WASHINGMACHINE_BUTTON);
  debounce(reading_dryer, DRYER_BUTTON);
  debounce(reading_oven, OVEN_BUTTON);
  debounce(reading_other, OTHER_BUTTON);
  lastButtonStateWashingmachine = reading_washingmachine;
  lastButtonStateDryer = reading_dryer;
  lastButtonStateOven = reading_oven;
  lastButtonStateOther = reading_other;

  // Some button power logic
  if (!isActive)
  {
    Serial.println("Setting active status");
    startTime = millis();
    isActive = true;
  }

  int reading_power_button = digitalRead(BUTTON_POWER_INPUT);
  if(millis() - startTime > 5000 && !reading_power_button)
  {
    digitalWrite(POWER_OUTPUT, LOW);
    client.publish("outTopic", "Shutting down");
  }

  // power_control();
}
