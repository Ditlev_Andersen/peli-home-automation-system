import paho.mqtt.client as mqtt
import time
import threading
import requests
from datetime import datetime

listOfActive = []
listOfOn = []


def getPricekWh():
    now = datetime.now()
    day = now.strftime("%d")
    month = now.strftime("%m")
    year = now.strftime("%Y")
    current_hour = now.strftime("%H")

    url = f"https://www.elprisenligenu.dk/api/v1/prices/{year}/{month}-{day}_DK2.json"
    response = requests.get(url)
    data = response.json()

    current_record = next(
        (
            record
            for record in data
            if record["time_start"].startswith(f"{year}-{month}-{day}T{current_hour}:")
        ),
        None,
    )

    return current_record.get("DKK_per_kWh")


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    #client.subscribe("inTopic")
    client.subscribe("home/appliance/dryer")
    client.subscribe("home/appliance/other")
    client.subscribe("home/appliance/oven")
    client.subscribe("home/appliance/washer")


def on_message(client, userdata, msg):
    # Keeping track of input from remote controller
    try:
        appliance = msg.topic[msg.topic.index("ce/")+3:]
        command = msg.payload.decode()
        print(f"Appliance: {appliance}    Command: {command}")
        # Commands from the remote controller
        if command == "activate":
            listOfActive.append(appliance)
        if command == "deactivate":
            listOfActive.remove(appliance)
        if command == "on":
            listOfOn.append(appliance)
            listOfActive.append(appliance)
        if command == "off":
            listOfOn.remove(appliance)

        # Messages from the ESPs
        if command == "stopped":
            listOfOn.remove(appliance)
            publish(client, appliance + "Topic", "stop")
    except:
        pass


def publish(client, topic, message):
    client.publish(topic, message)


def RPILoop(name):

    cheap_electricity = 0.7

    while True:
        price = getPricekWh()
        time.sleep(10)

        # Publish current electricity price for the NodeRed dashboard
        publish(client, "home/status/electricityPrice", price)

        # If the price is right -> turn on the appliances and tell the remote to toggle LEDs accordingly
        if price < cheap_electricity:
            #print("starting")
            for entry in listOfActive:
                # Start appliance
                print("starting: ", entry)
                publish(client, "home/appliance/" + entry , "true")

                # Tell remote control the appliance is started
                publish(client, "home/appliance/" + entry, "start")

                # Remove the started appliance from list
                try:
                    listOfActive.remove(entry)
                    listOfOn.append(entry)
                except:
                    pass

# RPI loop is run in a thread
x = threading.Thread(target=RPILoop, args=(1,))
x.start()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.enable_bridge_mode()

client.connect("192.168.1.110", 1883)
client.loop_forever()
