#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"

#include <WiFi.h>
#include <PubSubClient.h>

/* Networking: Wi-Fi */

//const char* ssid = "PELI_Sandbox";
//const char* password = "PELIpeli";

//const char* ssid = "SDU-NET";
//const char* password = "SDU-NET1";


const char* ssid = "MeFoun 11";
const char* password = "005memenet";


void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print("AA");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

/* Networking: MQTT */
 // "192.168.2.3";
//const char* mqtt_server = "192.168.1.199";
const char* mqtt_server = "172.20.10.5"; //"172.20.10.5"; //"10.126.128.57";//"172.20.10.5";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;



void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");

  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();
}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Subscribe
      client.subscribe("esp32/output");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
#include "SparkFun_VEML6030_Ambient_Light_Sensor.h"



/*  Sensor: Light sensor */


// Close the jumper on the product to use address 0x10.
#define AL_ADDR 0x48

SparkFun_Ambient_Light light(AL_ADDR);

// Possible values: .125, .25, 1, 2
// Both .125 and .25 should be used in most cases except darker rooms.
// A gain of 2 should only be used if the sensor will be covered by a dark
// glass.
float gain = .125;

// Possible integration times in milliseconds: 800, 400, 200, 100, 50, 25
// Higher times give higher resolutions and should be used in darker light. 
int time_period = 400;
long luxVal = 0; 

// Power save mode, options range from 1-4. 
// Default is 1.
int powMode = 2; 

/*  Sensor: Pressure/temp/humidity/air-quality sensor */

/*#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10*/

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME680 bme; // I2C
//Adafruit_BME680 bme(BME_CS); // hardware SPI
//Adafruit_BME680 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK);

void setup() {
  Serial.begin(115200);
  while (!Serial);
  Serial.println(("BME680 async test"));

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  client.publish("inTopic", "Hello world!");

  if (!bme.begin()) {
    Serial.println(F("Could not find a valid BME680 sensor, check wiring!"));
    while (1);
  }

  // Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms




  if(light.begin())
    Serial.println("Ready to sense some light!"); 
  else
    Serial.println("Could not communicate with the sensor!");

  // Again the gain and integration times determine the resolution of the lux
  // value, and give different ranges of possible light readings. Check out
  // hoookup guide for more info. 
  light.setGain(gain);
  light.setIntegTime(time_period);

  Serial.println("Reading settings..."); 
  Serial.print("Gain: ");
  float gainVal = light.readGain();
  Serial.print(gainVal, 3); 
  Serial.print(" Integration Time: ");
  int timeVal = light.readIntegTime();
  Serial.println(timeVal);

  // Power save mode enables low power consumption from your Ambient Light
  // Sensor. You can set modes from 1-4, four being the highest power saving
  // option but slowest refresh rate. The sensor's refresh rate is also
  // dependent on the integration time. For example a setting of 4 with an
  // integration time of 100ms == 4100ms refresh rate but 2 micro-amps current
  // draw. Check hookup guide for more info. 
  light.setPowSavMode(powMode);
  // Let's see that it was set correctly. 
  Serial.print("Power Save Mode: "); 
  int savVal = light.readPowSavMode();
  Serial.println(savVal);
  // light.enablePowSave();
  Serial.println("Is power save enabled: ");
  int enaVal = light.readPowSavEnabled(); 

  if(enaVal)
    Serial.println("Yes!");
  else
    Serial.println("No!");  

  // This will power down the sensor and the sensor will draw 0.5 micro-amps of
  // power while shutdown. 
  // light.shutDown();
  // light.powerOn();

  // Give some time to read your settings. 
  delay(1000);

}

void loop() {

    if (!client.connected()) {
    reconnect();
  }
  client.loop();
  // Tell BME680 to begin measurement.
  unsigned long endTime = bme.beginReading();
  if (endTime == 0) {
    Serial.println(F("Failed to begin reading :("));
    return;
  }
  Serial.print(F("Reading started at "));
  Serial.print(millis());
  Serial.print(F(" and will finish at "));
  Serial.println(endTime);

  Serial.println(F("You can do other work during BME680 measurement."));
  delay(50); // This represents parallel work.
  // There's no need to delay() until millis() >= endTime: bme.endReading()
  // takes care of that. It's okay for parallel work to take longer than
  // BME680's measurement time.

  // Obtain measurement results from BME680. Note that this operation isn't
  // instantaneous even if milli() >= endTime due to I2C/SPI latency.
  if (!bme.endReading()) {
    Serial.println(F("Failed to complete reading :("));
    return;
  }
  Serial.print(F("Reading completed at "));
  Serial.println(millis());

  Serial.print(F("Temperature = "));
  Serial.print(bme.temperature);
  Serial.println(F(" *C"));

  Serial.print(F("Pressure = "));
  Serial.print(bme.pressure / 100.0);
  Serial.println(F(" hPa"));

  Serial.print(F("Humidity = "));
  Serial.print(bme.humidity);
  Serial.println(F(" %"));

  Serial.print(F("Gas = "));
  Serial.print(bme.gas_resistance / 1000.0);
  Serial.println(F(" KOhms"));

  Serial.print(F("Approx. Altitude = "));
  Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
  Serial.println(F(" m"));

  Serial.println();
  //delay(2000);

    luxVal = light.readLight();
  Serial.print("Ambient Light Reading: ");
  Serial.print(luxVal);
  Serial.println(" Lux");  
  // Sampling at the rate specified in the hookup guide power save mode table. 

  char luxString[8];

  client.publish("inTopic", "I'm gonna sleep!");
  char humString[8];
  dtostrf(bme.humidity, 2, 2, humString);
  char tempString[8];
  dtostrf(bme.temperature, 2, 2, tempString);
  char pressureString[8];
  dtostrf(bme.pressure/100.0, 4, 2, pressureString);

  dtostrf(luxVal, 4, 0, luxString);


  String intString = String(luxVal);
  client.publish("room1/esp32/humidity", humString);
  client.publish("room1/esp32/temp", tempString);
   client.publish("room1/esp32/pres", pressureString);
  client.publish("room1/esp32/light", luxString);
  int num = 12;
  delay(2400);
}