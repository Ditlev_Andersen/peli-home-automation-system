This is the scripts used for all components of the PELI project Home Automation System

The RPI.py script is the code running on the Raspberry Pi. This facilitates communication between the system components

The LED_control.ino scrip is the arduino code for the remote controller

The remote_control and servo_control scripts are for the two "appliances" tested on the system. 

The plotPrice.py script is for plotting the price

The env_sensor_test.ino script is some testing for furtire work with an environmental sensor